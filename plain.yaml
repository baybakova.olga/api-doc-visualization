swagger: "2.0"
info:
  description: "This is a learning material for [Xsolla Summer School](https://school.xsolla.com/) program. Some <mark>requests</mark> are taken from the Petstore API documentation which can be found [here](https://petstore.swagger.io/). <br><br>Use this for practice and add your own paths and definitions. See the full task description in the [Google Doc](https://docs.google.com/document/d/1EDlrF0E0bgNc65ohrGCSoxJXhaNwWpcMiaMuRjfzOG4/edit?usp=sharing)."
  version: "1.0.0"
  title: "White your own methods"
host: "summer.school.xsolla.com"
basePath: "/api/v1"
tags:
- name: "pet"
  description: "Everything about your Pets"
  externalDocs:
    description: "Find out more"
    url: "http://swagger.io"
- name: "Xsolla"
  description: "Request from Xsolla API"
- name: "Stripe"
  description: "Request from Stripe API"
schemes:
- "https"
paths:
  /pet:
    post:
      tags:
      - "pet"
      summary: "Add a New Pet"
      description: "Adds a new pet to the store."
      operationId: "add-a-new-pet"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "Pet object that needs to be added to the store."
        required: true
        schema:
          $ref: "#/definitions/pet"
      responses:
        "200":
          description: "OK"
          schema:
            $ref: "#/definitions/pet"
        "400":
          description: "(Bad Request) Invalid request parameters"
          schema:
            $ref: "#/definitions/api_error"
          examples:
            application/json:
              code: "000-000"
              description: "Your request has bad parameters."
      security:
      - petstore_auth:
        - "write:pets"
        - "read:pets"
    put:
      tags:
      - "pet"
      summary: "Update an Pet"
      description: "Update information about the existing pet."
      operationId: "update-an-pet"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "Pet object that needs to be updated in the store."
        required: true
        schema:
          $ref: "#/definitions/pet"
      responses:
        "200":
          description: "OK"
          schema:
            $ref: "#/definitions/pet"
        "400":
          description: "(Bad Request) Invalid request parameters"
          schema:
            $ref: "#/definitions/api_error"
          examples:
            application/json:
              code: "000-000"
              description: "Your request has bad parameters."
        "404":
          description: "(Not Found) Requested resource not found"
          schema:
            $ref: "#/definitions/api_error"
          examples:
            application/json:
              code: "000-004"
              description: "Pet not found."
      security:
      - petstore_auth:
        - "write:pets"
        - "read:pets"
  /pet/{pet_id}:
    get:
      tags:
      - "pet"
      summary: "Get Pet by ID"
      description: "Gets the existing pet from the store by ID."
      operationId: "get-pet-by-id"
      produces:
      - "application/json"
      parameters:
      - name: "pet_id"
        in: "path"
        description: "Pet ID."
        required: true
        type: "integer"
        format: "int64"
      responses:
        "200":
          description: "OK"
          schema:
            $ref: "#/definitions/pet"
        "404":
          description: "(Not Found) Requested resource not found"
          schema:
            $ref: "#/definitions/api_error"
          examples:
            application/json:
              code: "000-004"
              description: "Pet not found."
        "429":
          description: "Validation exception"
          schema:
            $ref: "#/definitions/api_error"
          examples:
            application/json:
              code: "000-029"
              description: "Allowable number of requests exceeded."
      security:
      - api_key: []
    delete:
      tags:
      - "pet"
      summary: "Delete a Pet"
      description: "Deletes the pet by ID."
      operationId: "delete-a-pet"
      produces:
      - "application/json"
      parameters:
      - name: "api_key"
        in: "header"
        required: false
        type: "string"
      - name: "pet_id"
        in: "path"
        description: "Pet ID."
        required: true
        type: "integer"
        format: "int64"
      responses:
        "200":
          description: "OK"
        "404":
          description: "(Not Found) Requested resource not found"
          schema:
            $ref: "#/definitions/api_error"
          examples:
            application/json:
              code: "000-004"
              description: "Pet not found."
      security:
      - petstore_auth:
        - "write:pets"
        - "read:pets"
  "/projects/{project_id}/users/{user_id}/payment_accounts":
    get:
      tags:
      - "Xsolla" 
      summary: "Get Saved Accounts"
      description: "Lists saved payment accounts of a given user"
      operationId: "get-saved-accounts"
      produces:
      - "application/json"
      parameters:
      - name: "project_id"
        in: "path"
        description: "Project ID" 
        required: true
        type: "integer"
        format: "int64"
      - name: "user_id"
        in: "path"
        description: "User ID"
        required: true
        type: "integer"
        format: "int64"
      responses:
        "200":
          description: "OK"
          schema:
            type: "array"
            items: 
              $ref: "#/definitions/account"
        "404":
          description: "Not Found"
      security:
      - api_key: []
  /charges:
    post:
      tags:
      - "Stripe"
      summary: "Create a charge"
      description: "To charge a credit card or other payment source, you create a Charge object. If your API key is in test mode, the supplied payment source (e.g., card) won’t actually be charged, although everything else will occur as if in live mode. (Stripe assumes that the charge would have completed successfully)."
      operationId: "create-a-charge"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "body"
        in: "body"
        schema:
          type: object
          required:
            - "amount"
            - "currency"
          properties:
            amount:
              description: "Amount intended to be collected by this payment. A positive integer representing how much to charge in the smallest currency unit (e.g., 100 cents to charge $1.00 or 100 to charge ¥100, a zero-decimal currency). The minimum amount is $0.50 US or equivalent in charge currency. The amount value supports up to eight digits (e.g., a value of 99999999 for a USD charge of $999,999.99)"
              type: "number"
              format: "float"
              example: 100
            currency:
              description: "Three-letter ISO currency code, in lowercase. Must be a supported currency"
              type: "string"
              example: "usd"
            capture:
              description: "Whether to immediately capture the charge. Defaults to true. When false, the charge issues an authorization (or pre-authorization), and will need to be captured later. Uncaptured charges expire in seven days. For more information, see the authorizing charges and settling later documentation"
              type: "boolean"
              default: true
            receipt_email:
              description: "The email address to which this charge’s receipt will be sent. The receipt will not be sent until the charge is paid, and no receipts will be sent for test mode charges. If this charge is for a Customer, the email address specified here will override the customer’s email address. If receipt_email is specified for a charge in live mode, a receipt will be sent regardless of your email settings"
              type: "string"
              example: "my@email.com"
      responses:
        "200":
          description: "OK"
          schema:
            $ref: "#/definitions/charge"
      security:
      - api_key: []
securityDefinitions:
  petstore_auth:
    type: "oauth2"
    authorizationUrl: "http://petstore.swagger.io/oauth/dialog"
    flow: "implicit"
    scopes:
      write:pets: "modify pets in your account"
      read:pets: "read your pets"
  api_key:
    type: "apiKey"
    name: "api_key"
    in: "header"
definitions:
  account:
    type: "object"
    properties:
      id:
        type: "integer"
        format: "int64"
        example: 4612345
      name:
        type: "string"
        example: "PP_saved_account"
      payment_system:
        type: "object"
        properties:
          id:
            type: "integer"
            format: "int64"
            example: "24"
          name:
            type: "string"
            example: "PayPal"
      type:
        type: "string"
        example: "paypal"
  charge:
    type: "object"
    properties:
      id:
        type: "string"
        example: "ch_1HFWCi2eZvKYlo2CKud5k7FU"
      amount:
        type: "number"
        format: "float"
        example: "2000"
      billing_details:
        type: "object"
        properties:
          address:
            type: "object"
            properties:
              city:
                type: "string"
                example: "Perm"
              country:
                type: "string"
                example: "Russia"
      description:
        type: "string"
        example: "My First Test Charge (created for API docs)"
      paid:
        type: "boolean"
        example: true
  dog_breed:
    type: "object"
    properties:
      id:
        type: "integer"
        format: "int64"
        description: "Dog's breed ID in the store."
      name:
        type: "string"
        description: "Name of the dog's breed."
        example: 123
  pet:
    type: "object"
    required:
    - "id"
    - "name"
    - "dog_breed"
    properties:
      id:
        type: "integer"
        format: "int64"
        description: "Pet ID in the store."
        example: 123
      name:
        type: "string"
        description: "Pet name."
        example: "Bobik"
      dog_breed:
        $ref: "#/definitions/dog_breed"
      status:
        type: "string"
        description: "Pet status in the store."
        enum:
        - "available"
        - "pending"
        - "sold"
        example: "available"
  api_error:
    type: "object"
    description: "Error"
    required:
      - "code"
      - "description"
    properties:
      code:
        description: "Error code."
        type: "string"
      description:
        description: "Error description."
        type: "string"